package com.cursoandroid.notas.data.local.repository

import androidx.lifecycle.LiveData
import com.cursoandroid.notas.data.local.dao.NotesDao
import com.cursoandroid.notas.data.local.entity.NotesEntity

class NotesRespository(val notesDao: NotesDao) {

    fun allNotes():LiveData<List<NotesEntity>> = notesDao.getAllNotes()

    suspend fun insertNote(note: NotesEntity) = notesDao.insertNote(note)

    suspend fun updateNote(note: NotesEntity)=notesDao.updateNote(note)

    suspend fun deleteNote(id:Int) = notesDao.deleteNote(id)
}