package com.cursoandroid.notas.data.local.preferences

import android.content.Context

class LoginPreference(context: Context) {

    private val sharedPreferences =
        context.getSharedPreferences("loginPreference", Context.MODE_PRIVATE)

    fun putBoolean(key: String, flag: Boolean) =
        sharedPreferences.edit().putBoolean(key, flag).apply()


    fun getBoolean(key: String): Boolean {
        return sharedPreferences.getBoolean(key, false)
    }

    fun putString(key: String, value: String) {
        sharedPreferences.edit().putString(key, value).apply()
    }
}