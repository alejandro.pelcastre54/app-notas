package com.cursoandroid.notas.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.cursoandroid.notas.data.local.entity.NotesEntity

@Dao
interface NotesDao {

    @Query("SELECT * from notes_table ORDER BY timeStamp DESC")
    fun getAllNotes():LiveData<List<NotesEntity>>

    @Insert
    fun insertNote(note:NotesEntity)

    @Update
    fun updateNote (note:NotesEntity)

    @Query("DELETE FROM notes_table WHERE id = :idIdentifier")
    fun deleteNote(idIdentifier: Int)

}