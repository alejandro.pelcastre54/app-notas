package com.cursoandroid.notas.ui.view.home.fragments.note

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.cursoandroid.notas.R
import com.cursoandroid.notas.data.local.preferences.LoginPreference
import com.cursoandroid.notas.databinding.FragmentNoteBinding
import com.cursoandroid.notas.ui.adapters.ItemsNoteAdapter
import com.cursoandroid.notas.ui.adapters.NoteClickedListener
import com.cursoandroid.notas.ui.utils.Constants
import com.cursoandroid.notas.ui.view.home.activities.HomeActivity
import com.cursoandroid.notas.ui.view.login.LoginActivity
import com.cursoandroid.notas.ui.viewmodel.home.HomeViewModel

class NoteFragment : Fragment() {

    // ViewModel
    private val viewModel: HomeViewModel by viewModels()
    private lateinit var adapter: ItemsNoteAdapter
    private lateinit var preferences: LoginPreference

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentNoteBinding.inflate(inflater)

        binding.lifecycleOwner = this
        binding.vm = viewModel
        preferences = LoginPreference(requireContext()) // Contexto del Fragment

        // Vinculamos el adapter del recycler view con este fragment
        adapter = ItemsNoteAdapter(NoteClickedListener{
            findNavController().navigate(NoteFragmentDirections.actionNoteFragmentToUpdateFragment(it))

        })

        viewModel.allNotes.observe(viewLifecycleOwner) { notas ->
            adapter.submitList(notas)
        }

        binding.apply {
            binding.rvNotes.adapter = adapter
            fbAddNote.setOnClickListener {
                findNavController().navigate(NoteFragmentDirections.actionNoteFragmentToAddFragment())
            }

            fbCloseNote.setOnClickListener {
                preferences.putBoolean(Constants.KEY_ACTIVE_SESSION, false)

            //activity?.onBackPressed()
                val activeSession = Intent(requireContext(), LoginActivity::class.java)
                startActivity(activeSession)
            }
        }


        return binding.root
    }
}
