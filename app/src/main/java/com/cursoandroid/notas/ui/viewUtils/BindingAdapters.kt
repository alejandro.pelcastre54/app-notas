package com.cursoandroid.notas.ui.viewUtils

import android.widget.TextView
import androidx.databinding.BindingAdapter
import java.text.DateFormat
import java.util.*

@BindingAdapter("setTimeStamp")
fun setTimeStamp(date: TextView, timestamp:Long){
    date.text = DateFormat.getInstance().format(timestamp)
}