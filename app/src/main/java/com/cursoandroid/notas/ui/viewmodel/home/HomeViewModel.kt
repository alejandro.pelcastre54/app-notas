package com.cursoandroid.notas.ui.viewmodel.home

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.cursoandroid.notas.data.local.db.NotesRoomDatabase
import com.cursoandroid.notas.data.local.entity.NotesEntity
import com.cursoandroid.notas.data.local.repository.NotesRespository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeViewModel(application: Application) : AndroidViewModel(application) {

    // Database
    private val notesDao = NotesRoomDatabase.getDatabase(application).notesdDao()
    private val repository: NotesRespository
    val allNotes: LiveData<List<NotesEntity>>

    init {
        repository = NotesRespository(notesDao)
        allNotes = repository.allNotes()
    }

    fun insert(notes:NotesEntity){
       viewModelScope.launch(Dispatchers.IO){
           repository.insertNote(notes)
       }
    }

    //update and delete
    fun update(notes:NotesEntity){
        viewModelScope.launch(Dispatchers.IO){
            repository.updateNote(notes)
        }
    }

    fun delete(id:Int){
        viewModelScope.launch(Dispatchers.IO){
            repository.deleteNote(id)
        }
    }

}