package com.cursoandroid.notas.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.cursoandroid.notas.data.local.entity.NotesEntity
import com.cursoandroid.notas.databinding.ItemNotesBinding

class ItemsNoteAdapter(val listener: NoteClickedListener) :
    ListAdapter<NotesEntity, ItemsNoteAdapter.ViewHolder>(NotesDiffCallBack) {

    companion object NotesDiffCallBack : DiffUtil.ItemCallback<NotesEntity>() {
        override fun areItemsTheSame(oldItem: NotesEntity, newItem: NotesEntity) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: NotesEntity, newItem: NotesEntity) =
            oldItem == newItem
    }

    class ViewHolder(val binding: ItemNotesBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(note: NotesEntity, clickedListener: NoteClickedListener) {
            binding.notes = note
            binding.clickListener = clickedListener
            binding.executePendingBindings()

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemNotesBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem, listener)
    }
}

class NoteClickedListener(val listener: (notes: NotesEntity) -> Unit) {

    fun onClick(notes: NotesEntity) = listener(notes)
}