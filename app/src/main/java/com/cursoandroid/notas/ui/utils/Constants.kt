package com.cursoandroid.notas.ui.utils

object Constants {

    const val ERROR_EMPTY_FIELD = 1
    const val ERROR_INVALID_CREDENTIAL = 2
    const val ERROR_EMPTY_EMAIL = 3
    const val ERROR_EMPTY_PASSWORD = 4

    const val KEY_ACTIVE_SESSION = "activeSession"
    const val KEY_USER = "user"
}