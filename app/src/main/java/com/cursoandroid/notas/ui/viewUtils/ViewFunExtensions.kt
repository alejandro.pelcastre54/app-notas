package com.cursoandroid.notas.ui.viewUtils

import android.content.Context
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment

fun Context.toast(message:String){
    Toast.makeText(this,message,Toast.LENGTH_SHORT).show()
}

fun ProgressBar.show(){
    visibility = View.VISIBLE
}

fun ProgressBar.hide(){
    visibility = View.GONE
}

fun Fragment.toastFragment(message: String){
    Toast.makeText(requireContext(),message,Toast.LENGTH_SHORT).show()
}