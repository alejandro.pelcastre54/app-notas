package com.cursoandroid.notas.ui.viewmodel.login

import android.app.Application
import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import com.cursoandroid.notas.model.interfacs.LoginListener
import com.cursoandroid.notas.ui.utils.Constants

class LoginViewModel(application: Application) : AndroidViewModel(application) {

    var listener: LoginListener? = null

    val userEmail = ObservableField<String>("")
    val userPassword = ObservableField<String>("")

    fun validateFields(view: View) {
        listener?.showProgressBar()

        if (!userEmail.get().isNullOrEmpty() && !userPassword.get().isNullOrEmpty()) {
            if( userEmail.get() == "carlos.torres@mobilestudio.com" && userPassword.get() == "12345" ){
                listener?.onSuccess(userEmail.get())
            }else{
                listener?.onErrorLogin(Constants.ERROR_INVALID_CREDENTIAL)
            }

        } else {
            if (userEmail.get().isNullOrEmpty() && !userPassword.get().isNullOrEmpty()) {
                listener?.onErrorLogin(Constants.ERROR_EMPTY_EMAIL)
            } else if (userPassword.get().isNullOrEmpty() && !userEmail.get().isNullOrEmpty()) {
                listener?.onErrorLogin(Constants.ERROR_EMPTY_PASSWORD)
            } else {
                listener?.onErrorLogin(Constants.ERROR_EMPTY_FIELD)
            }
        }
        listener?.hideProgressBar()
    }


}