package com.cursoandroid.notas.ui.view.home.fragments.add

import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.cursoandroid.notas.R
import com.cursoandroid.notas.data.local.entity.NotesEntity
import com.cursoandroid.notas.databinding.FragmentAddBinding
import com.cursoandroid.notas.ui.adapters.ItemsNoteAdapter
import com.cursoandroid.notas.ui.viewmodel.home.HomeViewModel
import kotlinx.android.synthetic.main.fragment_add.*

class AddFragment : Fragment() {

    private val viewModel:HomeViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = FragmentAddBinding.inflate(inflater)


        binding.apply{
            val titleNote = binding.etTitle.text
            val descriptionNote = etDescription.text

            btnSave.setOnClickListener {
                if ( TextUtils.isEmpty(et_title.text) && TextUtils.isEmpty(descriptionNote) ){
                    Toast.makeText(requireContext(),getString(R.string.msg_empty_fields),Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }

                val addNote = NotesEntity(
                    0,
                    titleNote.toString(),
                    descriptionNote.toString(),
                    System.currentTimeMillis()
                )

                // Guardamaos en la base de datos
                viewModel.insert(addNote)
                Toast.makeText(requireContext(),getString(R.string.msg_ad_successful),Toast.LENGTH_SHORT).show()
                findNavController().navigate(R.id.action_add_fragment_to_note_fragment)
            }
        }

        return binding.root
    }
}