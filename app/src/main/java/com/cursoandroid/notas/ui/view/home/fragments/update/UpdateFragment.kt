package com.cursoandroid.notas.ui.view.home.fragments.update

import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.cursoandroid.notas.R
import com.cursoandroid.notas.data.local.entity.NotesEntity
import com.cursoandroid.notas.databinding.FragmentUpdateBinding
import com.cursoandroid.notas.ui.adapters.ItemsNoteAdapter
import com.cursoandroid.notas.ui.viewUtils.toast
import com.cursoandroid.notas.ui.viewUtils.toastFragment
import com.cursoandroid.notas.ui.viewmodel.home.HomeViewModel

class UpdateFragment : Fragment() {

    private val viewModel: HomeViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentUpdateBinding.inflate(inflater)
        val args = UpdateFragmentArgs.fromBundle(requireArguments()).datosNota

        binding.apply {
            etTitleUpdate.setText(args.title)
            etDescriptionUpdate.setText(args.description)

            btnUpdate.setOnClickListener {
                if (TextUtils.isEmpty(etTitleUpdate.text) && TextUtils.isEmpty(etDescriptionUpdate.text)) {
                    toastFragment(getString(R.string.msg_empty_fields))
                    return@setOnClickListener
                }
                val note = NotesEntity(
                    args.id,
                    etTitleUpdate.text.toString(),
                    etDescriptionUpdate.text.toString(),
                    args.timeStamp
                )
                viewModel.update(note)
                //context?.toast(getString(R.string.msg_update_successful))
                toastFragment(getString(R.string.msg_update_successful))
                //Toast.makeText(requireContext(), getString(R.string.msg_update_successful), Toast.LENGTH_SHORT).show()
                findNavController().navigate(R.id.action_update_fragment_to_note_fragment)
            }

            btnDelete.setOnClickListener {
                viewModel.delete(args.id)
                toastFragment(getString(R.string.msg_delete))
                findNavController().navigate(R.id.action_update_fragment_to_note_fragment)
            }
        }
        return binding.root
    }
}