package com.cursoandroid.notas.ui.view.home.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.cursoandroid.notas.R
import com.cursoandroid.notas.ui.viewUtils.toast

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }
}