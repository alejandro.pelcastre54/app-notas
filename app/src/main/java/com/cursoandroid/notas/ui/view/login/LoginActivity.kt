package com.cursoandroid.notas.ui.view.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.cursoandroid.notas.R
import com.cursoandroid.notas.data.local.preferences.LoginPreference
import com.cursoandroid.notas.databinding.ActivityLoginBinding
import com.cursoandroid.notas.model.interfacs.HomeListener
import com.cursoandroid.notas.model.interfacs.LoginListener
import com.cursoandroid.notas.ui.utils.Constants
import com.cursoandroid.notas.ui.view.home.activities.HomeActivity
import com.cursoandroid.notas.ui.viewUtils.hide
import com.cursoandroid.notas.ui.viewUtils.show
import com.cursoandroid.notas.ui.viewUtils.toast
import com.cursoandroid.notas.ui.viewmodel.login.LoginViewModel
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), LoginListener {

    // Generamos la conexión - DataBinding -> ViewModel
    private lateinit var binding: ActivityLoginBinding
    private lateinit var viewModel: LoginViewModel
    private var constants: Constants = Constants

    // SharedPreferences
    private lateinit var preferences: LoginPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Databinding
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
        binding.vm = viewModel
        viewModel.listener = this

        // Preferences
        preferences = LoginPreference(applicationContext)
        validateActiveSession()
    }

    private fun validateActiveSession() {
        if (preferences.getBoolean(constants.KEY_ACTIVE_SESSION)) {
            val activeSession = Intent(this, HomeActivity::class.java)
            startActivity(activeSession)
            finish()
        }
    }

    override fun onSuccess(user: String?) {
        val intent = Intent(this, HomeActivity::class.java)
        preferences.putBoolean(constants.KEY_ACTIVE_SESSION, true)
        preferences.putString(constants.KEY_USER,user!!)

        startActivity(intent)

        finish()

    }

    override fun onErrorLogin(errorType: Int) {
        when (errorType) {
            constants.ERROR_EMPTY_EMAIL -> toast(getString(R.string.msg_empty_email))
            constants.ERROR_EMPTY_PASSWORD -> toast(getString(R.string.msg_empty_password))
            constants.ERROR_EMPTY_FIELD -> toast(getString(R.string.msg_empty_fields))
            constants.ERROR_INVALID_CREDENTIAL -> toast(getString(R.string.msg_invalid_credentials))
        }
    }

    override fun showProgressBar() {
        progressLogin.show()
    }

    override fun hideProgressBar() {
        progressLogin.hide()
    }
}