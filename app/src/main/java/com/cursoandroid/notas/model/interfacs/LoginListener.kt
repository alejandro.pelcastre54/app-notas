package com.cursoandroid.notas.model.interfacs

interface LoginListener {

    fun onSuccess (user:String?)
    fun onErrorLogin(errorType:Int)
    fun showProgressBar()
    fun hideProgressBar()

}